//
//  swiftLingo-Bridging-Header.h
//  swiftLingo
//
//  Created by lingo on 2017/11/24.
//  Copyright © 2017年 livefor. All rights reserved.
//

#ifndef swiftLingo_Bridging_Header_h
#define swiftLingo_Bridging_Header_h
//#import <QiniuSDK>
@import UIKit;
#import "AFNetworking/AFNetworking.h"
#import "Qiniu/QiniuSDK.h"
#import "HappyDNS/HappyDNS.h"
#import "WechatOpenSDK/WXApiObject.h"
#import "WechatOpenSDK/WXApi.h"

#endif /* swiftLingo_Bridging_Header_h */

