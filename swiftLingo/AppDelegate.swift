//
//  AppDelegate.swift
//  swiftLingo
//
//  Created by lingo on 2017/10/7.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import Alamofire
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,WXApiDelegate{

    var window: UIWindow?
 
    var netReachabilityManager: NetworkReachabilityManager?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window?.frame = UIScreen.main.bounds
        self.window?.makeKeyAndVisible()
        
        //微信配置
        WXConfigure()
        
        addNoti()
        
        changeRootToLoginVC()
        //changeRootToTabBarVC()
        //changeRootToTestVC()
        //检测网络状态
        checkNetReachable()
        
        return true
    }
    
    /// 检测网络状态
    func checkNetReachable() -> Void {
        self.netReachabilityManager = NetworkReachabilityManager(host: "http://alamofire.org/")
        self.netReachabilityManager?.listener = { status in
            switch status {
            case .notReachable://不可达
                print("notReachable")
            case .unknown://未知
                print("unknown")
            case .reachable(.ethernetOrWiFi)://以太网或者wifi
                print("ethernetOrWiFi")
            case .reachable(.wwan)://无线广域网
                print("wwan")
            }
        }
        netReachabilityManager?.startListening()
    }
    
    
    func addNoti(){
        kNotiCenter.addObserver(self, selector: #selector(changeRootToLoginVC), name: NSNotification.Name(rawValue: changeRootToLoginKey), object: nil)
        kNotiCenter.addObserver(self, selector: #selector(changeRootToTabBarVC), name: NSNotification.Name(rawValue: changeRootToTabbarKey), object: nil)
    }
    @objc func changeRootToLoginVC() {
        let loginVC = LGLoginViewController()
        let loginNav = LGLoginNavController.init(rootViewController: loginVC)
        self.window?.rootViewController = loginNav
    }
    @objc func changeRootToTabBarVC() {
        let tabBarVC = LGTabBarController()
        self.window?.rootViewController = tabBarVC
    }
    
    func changeRootToTestVC() -> Void {
        let testVC = LGDrawingBoardViewController()//LGTestViewController()
        self.window?.rootViewController = testVC
    }
    
    func WXConfigure() ->Void {
        WXApi.registerApp(wxAppId)
        
    }
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return WXApi.handleOpen(url, delegate: self)
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return WXApi.handleOpen(url, delegate: self)
    }
    func onReq(_ req: BaseReq!) {
        LGLog(req)
    }
    func onResp(_ resp: BaseResp!) {
        LGLog(resp)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

