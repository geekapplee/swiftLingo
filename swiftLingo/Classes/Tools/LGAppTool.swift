//
//  LGAppTool.swift
//  collectionDemo
//
//  Created by lingo on 2017/8/2.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

///屏幕高度
let kScreenH : CGFloat = UIScreen.main.bounds.size.height
///屏幕宽度
let kScreenW : CGFloat = UIScreen.main.bounds.size.width
///导航栏高度
let kNavHeight:CGFloat = 64
///tabbar高度
let kTabBarH:CGFloat = 49
///通知中心
let kNotiCenter = NotificationCenter.default

let kUserDefaults = UserDefaults.standard

let kApplication = UIApplication.shared

let kKeyWindow = kApplication.keyWindow
///key value 区域
let changeRootToLoginKey = "changeRootToLoginKey"
let changeRootToTabbarKey = "changeRootToTabbarKey"

let SUCCESS_CODE = 0
let DATA_ERROR = 10004

let hudDelay = 0.4

func LGLog<T>(_ messsage : T, file : String = #file, funcName : String = #function, lineNum : Int = #line) {
    #if DEBUG
        let fileName = (file as NSString).lastPathComponent
        print("\(fileName):[Line\t\(lineNum)] \(messsage)")
    #endif
}

//func placeholderColor() -> UIColor {
//    return UIColor.init(hexString: "9EA5AB")!
//}
//App背景色
var bgColor: UIColor{return UIColor.init(hexString: "CBE9D0")!}
///首页登录的占位文字颜色
var placeholderColor: UIColor{return UIColor.init(hexString: "cccccc")!}

///tabbar 非选中和选中状态的颜色
var tabBarNormalColor: UIColor {return UIColor(hex: "666666")}
var tabBarSelectedColor: UIColor{return UIColor(hex: "5B91FA")}

///baseURL

//生成随机颜色测试用
func randomColor()->UIColor{
    #if DEBUG
        return UIColor.init(red:CGFloat(arc4random_uniform(255))/CGFloat(255.0), green:CGFloat(arc4random_uniform(255))/CGFloat(255.0), blue: CGFloat(arc4random_uniform(255))/CGFloat(255.0),alpha: 1.0)
    #else
        return UIColor.clear
    #endif
}
//生成随机句子测试用 默认随机是10个字 传0 表示随机
func randomText(_ count: Int = 10) -> String {
    #if DEBUG
    let text = "的一了是我不在人们有来他这上着个地到大里说就去子得也和那要下看天时过出小么起你都把好还多没为又可家学只以主会样年想生同老中十从自面前头道它后然走很像见两用她国动进成回什边作对开而己些现山民候经发工向事命给长水几义三声于高手知理眼志点心战二问但身方实吃做叫当住听革打呢真全才四已所敌之最光产情路分总条白话东席次亲如被花口放儿常气五第使写军吧文运再果怎定许快明行因别飞外树物活部门无往船望新带队先力完却站代员机更九您每风级跟笑啊孩万少直意夜比阶连车重便斗马哪化太指变社似士者干石满日决百原拿群究各六本思解立河村八难早论吗根共让相研今其书坐接应关信觉步反处记将千找争领或师结块跑谁草越字加脚紧爱等习阵怕月青半火法题建赶位唱海七女任件感准张团屋离色脸片科倒睛利世刚且由送切星导晚表够整认响雪流未场该并底深刻平伟忙提确近亮轻讲农古黑告界拉名呀土清阳照办史改历转画造嘴此治北必服雨穿内识验传业菜爬睡兴形量咱观苦体众通冲合破友度术饭公旁房极南枪读沙岁线野坚空收算至政城劳落钱特围弟胜教热展包歌类渐强数乡呼性音答哥际旧神座章帮啦受系令跳非何牛取入岸敢掉忽种装顶急林停息句区衣般报叶压慢叔背细"
    let textLength = text.characters.count
    var charCount = count//防止越界
    if charCount>textLength {
        charCount = textLength
    }
    //为0 的时候
    let randomIndex:Int = Int(arc4random_uniform(UInt32(textLength)))
    let startRandomIndex:Int = Int(arc4random_uniform(UInt32(textLength-charCount)))
    var startIndex = text.index(text.startIndex, offsetBy: 0)
    var endIndex = text.index(text.startIndex,offsetBy:randomIndex);
    if count != 0 {//需求位数不为0的时候
        startIndex = text.index(text.startIndex, offsetBy: startRandomIndex)
        endIndex = text.index(text.startIndex, offsetBy: startRandomIndex + count)
    }
    let res = text.substring(with: startIndex..<endIndex)
    return res
    #else
        return ""
    #endif
}

func kColor(r:CGFloat,g:CGFloat,b:CGFloat) -> UIColor {
    return UIColor.init(red: r, green: g, blue: b, alpha: 1.0)
}

class LGAppTool: NSObject {

}
