//
//  LGLayerExtension.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/9.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation
import UIKit
extension CALayer {
    public var lg_y : CGFloat {
        get {
            return self.frame.origin.y
        }
        set(newValue){
            var fr = self.frame
            fr.origin.y = newValue
            self.frame = fr
        }
    }
}
