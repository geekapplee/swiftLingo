//
//  LGIconFonts.swift
//  SwiftIcons
//
//  Created by lingo on 2017/10/9.
//

import Foundation


//MARK: 添加字体库支持 start
public enum FontType: FontProtocol {
    /// It selects icofont icon for the particular object from the library
    case iconfont(iconfontType)
    
    /**
     This function returns the font name using font type
     返回字体名
     */
    func fontName() -> String {
        var fontName: String
        switch self {
        case .iconfont(_):
            fontName = "iconfont"
            break
        }
        return fontName
    }
    
    /**
     This function returns the file name from Source folder using font type
     返回文件名
     */
    func fileName() -> String {
        var fileName: String
        switch self {
        case .iconfont(_):
            fileName = "iconfont"
            break
        }
        return fileName
    }
    
    /**
     This function returns the font family name using font type
     返回family名
     */
    func familyName() -> String {
        var familyName: String
        switch self {
        case .iconfont(_):
            familyName = "iconfont"
            break
        }
        return familyName
    }
    
    /**
     This function returns the error for a font type
     错误返回
     */
    func errorAnnounce() -> String {
        let message = " FONT - not associated with Info.plist when manual installation was performed";
        let fontName = self.fontName().uppercased()
        let errorAnnounce = fontName.appending(message)
        return errorAnnounce
    }
    
    /**
     This function returns the text for the icon
     */
    public var text: String? {
        var text: String

        switch self {
        case let .iconfont(icon):
            text = icon.text!
            break
        }
        return text
    }
}
//MARK: 添加字体库支持 end

//MARK:  1.------------阿里巴巴icons------------------
public enum iconfontType: Int{
    //数量
    static var count: Int {
        return iconfonts.count
    }
    public var text: String? {
        return iconfonts[rawValue]
    }
    case shouye,dongtai,rightMore,account,back
}
private let iconfonts = ["\u{e64f}","\u{e6b3}","\u{e65f}","\u{e6b9}","\u{e697}"]

//MARK:  2.------------其他icons------------------







