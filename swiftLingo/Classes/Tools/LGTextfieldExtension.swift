//
//  LGTextfieldExtension.swift
//  swiftLingo
//
//  Created by lingo on 2017/10/8.
//  Copyright © 2017年 livefor. All rights reserved.
//  给textfiled 增加下划线

import Foundation
import UIKit

extension UITextField{
    func setUnderline(color underlineColor: UIColor,fixX leftFixMargin: CGFloat = 0,height underlineHeight: CGFloat = 1 ){
        guard self.superview != nil else {
            return
        }
        let underline = UIView.init()
        underline.backgroundColor = underlineColor
        self.superview?.addSubview(underline)
        underline.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.bottom)
            make.left.equalTo(self).offset(leftFixMargin)
            make.right.equalTo(self).offset(-leftFixMargin)
            make.height.equalTo(underlineHeight)
        }
    }
}
