//
//  LGSharable.swift
//  swiftLingo
//
//  Created by lingo on 2017/12/19.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation

protocol LGSharable {
    
}
extension LGSharable {
    
    /// 文字分享到微信
    ///
    /// - Parameters:
    ///   - text: 文字内容
    ///   - inScene: 请求发送场景
    /// - Returns: 是否分享成功
    @discardableResult
    func sendTextContent(text: String, inScene: WXScene,bText:Bool = true, message:WXMediaMessage) -> Bool {
        let req = SendMessageToWXReq()
        req.bText = bText
        if bText {
           req.text = text
        }else{
            req.message = message;
        }
        req.scene = Int32(inScene.rawValue)
        return WXApi.send(req)
    }
    
    func sendImg() -> Void {
        
    }
    
    
    
}
