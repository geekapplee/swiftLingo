//
//  LGBarButtonItem.swift
//  Play
//
//  Created by lingo on 2017/8/14.
//  Copyright © 2017年 letus. All rights reserved.
//  快捷设置导航栏的item

import UIKit
extension UIBarButtonItem {
    convenience init(title:String?, target:Any, action:Selector){
        self.init(title: title, img: nil, target: target, action: action)
    }
    
    convenience init(backTarget:Any, action:Selector) {
        let img = UIImage.init(icon: .iconfont(.back), size: CGSize.init(width: 40, height: 40))
        let edg = UIEdgeInsets.init(top: 0, left: -20, bottom: 0, right: 0)
        self.init(title: "", img: img, edgeInset: edg, target: backTarget, action: action)
    }

    convenience init(title:String?, img:UIImage?,target:Any, action:Selector){
        self.init(title: title, img: img, edgeInset: UIEdgeInsets.zero, target: target, action: action)
    }
    
    convenience init(title:String?, img:UIImage?,edgeInset:UIEdgeInsets,target:Any, action:Selector){
        let navBtn = UIButton.init(type: .custom)
        navBtn.setTitle(title, for: .normal)
        navBtn.setTitleColor(.gray, for: .normal)
        navBtn.setImage(img, for: .normal)
        navBtn.sizeToFit()
        navBtn.contentEdgeInsets = edgeInset
        navBtn.addTarget(target, action: action, for: .touchUpInside)
        self.init(customView: navBtn)
    }
    
    
//    convenience init(shutdownTarget:Any,action:Selector) {
//        
//    }
}
