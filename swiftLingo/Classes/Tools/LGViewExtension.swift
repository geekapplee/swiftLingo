//
//  LGViewExtension.swift
//  collectionDemo
//
//  Created by lingo on 2017/8/2.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    public var lg_x : CGFloat {
        get {
            return self.frame.origin.x
        }
        set(newValue){
            var fr = self.frame
            fr.origin.x = newValue
            self.frame = fr
        }
    }
    
    public var lg_y : CGFloat {
        get {
            return self.frame.origin.y
        }
        set(newValue){
            var fr = self.frame
            fr.origin.y = newValue
            self.frame = fr
        }
    }
    
    public var lg_width : CGFloat {
        get {
            return self.frame.size.width
        }
        set(newValue){
            var fr = self.frame
            fr.size.width = newValue
            self.frame = fr
        }
    }
    
    public var lg_height : CGFloat {
        get {
            return self.frame.size.height
        }
        set(newValue){
            var fr = self.frame
            fr.size.height = newValue
            self.frame = fr
        }
    }
    
    public var lg_size : CGSize {
        get{
            return self.frame.size
        }
        set(newValue){
            var fr = self.frame
            fr.size = newValue
            self.frame = fr
        }
    }
    
    public var lg_origin : CGPoint {
        get{
            return self.frame.origin
        }
        set(newValue){
            var fr = self.frame
            fr.origin = newValue
            self.frame = fr 
        }
    }
}


