//
//  LGUserModel.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/7.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation
import HandyJSON
struct LGUserModel : HandyJSON {
    var id : Int = 0
    var userIcon : String = ""
    var username : String = ""
}
