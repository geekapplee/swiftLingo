//
//  LGTabBarController.swift
//  lingo
//
//  Created by lingo on 2017/9/22.
//  Copyright © 2017年 livefor. All rights reserved.
//  标签控制器


import UIKit

class LGTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //全局设置tabbar的属性
        self.tabBar.isTranslucent = false
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: tabBarNormalColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: tabBarSelectedColor], for: .selected)
        
        addChildrenVC()
    }
    
    func addChildrenVC() -> Void {
        addChildVcs(childVC:LGHomeViewController(), title: "首页", fontType: .iconfont(.shouye))
        addChildVcs(childVC:LGDynamicViewController(), title: "动态", fontType: .iconfont(.dongtai))
        addChildVcs(childVC:LGMeViewController(), title: "我的", fontType: .iconfont(.account))
    }
    
    func addChildVcs(childVC: UIViewController, title: String, fontType: FontType) -> Void {
        childVC.title = title
        childVC.tabBarItem.setIcon(icon: fontType,textColor: tabBarNormalColor, selectedTextColor: tabBarSelectedColor)
        let navRoot = LGNavigationController.init(rootViewController: childVC)
        self.addChildViewController(navRoot)
    }
}

