//
//  ViewController.swift
//  lingo
//
//  Created by lingo on 2017/9/12.
//  Copyright © 2017年 livefor. All rights reserved.
//  所有类的基类

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    //内存回收
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = bgColor;

    }
}

