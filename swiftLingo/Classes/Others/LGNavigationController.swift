//
//  LGNavigationController.swift
//  lingo
//
//  Created by lingo on 2017/9/22.
//  Copyright © 2017年 livefor. All rights reserved.
//  导航控制器

import UIKit

class LGNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        //一个或者一个以上
        if childViewControllers.count>0 {
            //1.隐藏底部
            viewController.hidesBottomBarWhenPushed = true
            //2.添加默认的返回按钮
            viewController.navigationItem.leftBarButtonItem =  UIBarButtonItem.init(backTarget: self, action: #selector(back))
        }
        super.pushViewController(viewController, animated: animated)
    }
    
    @objc func back(){
        popViewController(animated: true)
    }
}
