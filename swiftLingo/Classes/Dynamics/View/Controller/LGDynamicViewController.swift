//
//  LGDynamicViewController.swift
//  lingo
//
//  Created by lingo on 2017/9/26.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
class LGDynamicViewController: ViewController {
    fileprivate lazy var mainTableView = { () -> UITableView in 
        let mainTableView = UITableView.init(frame: self.view.bounds, style: .plain)
        mainTableView.backgroundColor = bgColor
        mainTableView.dataSource = self
        mainTableView.delegate = self
        mainTableView.tableFooterView = UIView()
        mainTableView.separatorStyle = .none
        mainTableView.contentInset = UIEdgeInsetsMake(kNavHeight, 0, kTabBarH, 0)
        mainTableView.registerReusable(cellType: LGDynamicCell.self)
        return mainTableView
    }()
    
    fileprivate lazy var dynamicVM = LGDynamicViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11, *) {
            mainTableView.contentInsetAdjustmentBehavior = .never
        }else{
            automaticallyAdjustsScrollViewInsets = false
        }
        view.addSubview(mainTableView)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "请求", target: self, action: #selector(testRequst))
        
        //UIPageViewController
    }
    @objc func testRequst() {
        dynamicVM.requestData {
            self.mainTableView.reloadData()
        }
    }

}

extension LGDynamicViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dynamicVM.listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withCellType: LGDynamicCell.self, indexPath: indexPath)
        let layout = dynamicVM.layoutArray[indexPath.row]
        cell.layout = layout
        return cell
    }
}

extension LGDynamicViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let layout:LGDynamicLayout = dynamicVM.layoutArray[indexPath.row]
        return layout.totalHeight
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}











