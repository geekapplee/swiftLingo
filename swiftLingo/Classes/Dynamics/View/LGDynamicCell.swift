//
//  LGDynamicCell.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/7.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import Kingfisher
class LGDynamicCell: UITableViewCell, LGReusable{
    //动态父控件
    let dynamicView = LGDynamicView()
    var layout:LGDynamicLayout!{
        didSet{
            dynamicView.layout = layout
        }
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = bgColor
        contentView.addSubview(dynamicView)
        dynamicView.frame = CGRect(x: 0, y: cellMargin, width: kScreenW, height: 1)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

///所有子控件的父控件
class LGDynamicView:UIView {
    let titleView = LGDynamicTitleView()
    var contentTextLabel = {() -> UILabel in
        let contentTextLabel = UILabel.init()
        contentTextLabel.font = contentTextFont
        contentTextLabel.numberOfLines = 0
        contentTextLabel.backgroundColor = UIColor.lightText
        return contentTextLabel
    }()
    
    var sepLine:CALayer = {
        let sepLine = CALayer()
        sepLine.frame = CGRect.init(x: 0, y: 0, width:kScreenW, height: 1)
        sepLine.backgroundColor = UIColor.init(white: 0, alpha: 0.09).cgColor
        return sepLine
    }()
    
    var layout:LGDynamicLayout!{
        didSet{
            self.lg_height = layout.totalHeight - cellMargin
            //头部布局
            titleView.layout = layout
            //分割layer
            sepLine.lg_y = titleView.frame.maxY
            
           //文字布局
            contentTextLabel.lg_x = leftPadding
            contentTextLabel.lg_width = contentTextW
            contentTextLabel.lg_height = layout.contentTextLabelH
            contentTextLabel.lg_y = sepLine.frame.maxY
            
            contentTextLabel.text = layout._dynamic?.content
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        //1.头部控件 头像 名字..
        addSubview(titleView)
        //分割线
        layer.addSublayer(sepLine)
        //2.主体文字
        addSubview(contentTextLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

///头部控件
class LGDynamicTitleView: UIView {
    let avatarView = { () -> UIImageView in
        let avatarView = UIImageView.init()
        avatarView.backgroundColor = randomColor()
        avatarView.lg_size = CGSize(width: avatarWH, height: avatarWH)
        avatarView.lg_origin = CGPoint(x: cellMargin, y: cellMargin)
       // avatarView.contentMode = .center
        avatarView.layer.masksToBounds = true
        avatarView.layer.cornerRadius = CGFloat(avatarWH)/2.0
        avatarView.layer.shouldRasterize = true
        avatarView.layer.rasterizationScale = UIScreen.main.scale
        return avatarView
    }()

    let usernameLabel = { () -> UILabel in
        let usernameLabel = UILabel.init()
       // usernameLabel.backgroundColor = UIColor.blue
        usernameLabel.lg_width = namelabelW //kScreenW - avatarWH - leftPadding - rigthPadding - 8
        usernameLabel.lg_height = 24
        return usernameLabel
    }()
    
    let timeLabel = { () -> UILabel in
        let timeLabel = UILabel.init()
        timeLabel.lg_width = namelabelW
        timeLabel.lg_height = 24
        return timeLabel
    }()
    
    var layout:LGDynamicLayout!{
        didSet{
            usernameLabel.text = layout._dynamic?.user.username
            let date =  Date()
            let f = DateFormatter.init()
            f.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateStr = f.string(from: date)
            timeLabel.text = dateStr
            if let userIconStr = layout._dynamic?.user.userIcon {
                let userIconUrl = URL(string: userIconStr)
                avatarView.kf.setImage(with: userIconUrl)
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.lg_height = titleViewH
        self.lg_width = kScreenW
        //1. 头像
        addSubview(avatarView)
        //2. 用户名
        addSubview(usernameLabel)
        usernameLabel.lg_x = avatarView.frame.maxX + 8
        usernameLabel.lg_y = avatarView.frame.minY

        //3. 时间
        addSubview(timeLabel)
        timeLabel.lg_x = usernameLabel.frame.minX
        timeLabel.lg_y = usernameLabel.frame.maxY
    }

    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



