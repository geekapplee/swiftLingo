//
//  LGDynamicLayout.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/7.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
enum LGLayoutStyle {
    case timeLine
    case detail
}
let cellMargin: CGFloat = 8 //上边距
let titleViewH: CGFloat = 56  // 头部父控件的高度
let avatarWH : CGFloat = 40 //头像的宽高
let cellPadding : CGFloat = 12 //cell的padding
let leftPadding : CGFloat = 8  //左padding
let rigthPadding : CGFloat = 8//右padding
//
let contentTextFont = UIFont.systemFont(ofSize: 18)

//
var namelabelW : CGFloat {return kScreenW - avatarWH - leftPadding - rigthPadding - 8}
var contentTextW : CGFloat {return kScreenW - leftPadding - rigthPadding}

class LGDynamicLayout: NSObject {
    
    var totalHeight: CGFloat = 0 // 总高度
    var contentTextLabelH : CGFloat = 0 //正文的高度
    var _dynamic:LGDynamicModel?
    var _dynamicStyle:LGLayoutStyle?
    
    func initWithDynamicModel(dynamic: LGDynamicModel,style: LGLayoutStyle = .timeLine) -> Self {
        _dynamic = dynamic
        _dynamicStyle = style
        layout()
        return self
    }
    
    func layout() {
        //1.分计算
        self.layoutTitleView()
        
        //计算总高度
        totalHeight = 0
        totalHeight += cellMargin // 加上cell的间距
        totalHeight += 1  //加上分割线
        totalHeight += titleViewH //头部个人名片的高度
        totalHeight += contentTextLabelH//主体文字的高度
       // totalHeight += cellMargin
    }
    
    func layoutTitleView(){
        let text : String = (_dynamic?.content)!
        let textH = (text as NSString).boundingRect(with: CGSize.init(width: contentTextW, height: CGFloat(MAXFLOAT)), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font:contentTextFont], context: nil).size.height
        contentTextLabelH = textH;
    }
}
