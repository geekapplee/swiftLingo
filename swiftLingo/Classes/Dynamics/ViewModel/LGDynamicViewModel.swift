//
//  LGDynamicViewModel.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/3.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import HandyJSON
class LGDynamicViewModel: NSObject,LGRequestable{
    var urlString: String = "/livefor/dynamic"
    var type: MethodType = .get
    var parameters: [String : Any] = [:]
    
    var listArray : [LGDynamicModel] = {return NSMutableArray.init()}() as! [LGDynamicModel]
    
    var layoutArray : [LGDynamicLayout] = [LGDynamicLayout]()
}

extension LGDynamicViewModel {
    func parseResult(_ result: Any) {
        let objArray = [LGDynamicModel].deserialize(from: result as? NSArray)
        if (objArray?.count)!<=0{return}
        listArray = objArray! as! [LGDynamicModel]
        for dynamic in listArray {
            let layout = LGDynamicLayout().initWithDynamicModel(dynamic: dynamic, style: .timeLine)
            layoutArray.append(layout)
        }
    }
}
