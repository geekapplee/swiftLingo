
//
//  LGDynamicModel.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/7.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation
import HandyJSON

struct LGDynamicModel : HandyJSON {
    var id: Int = 0
    var content: String = ""
    var msgType: Int = 0
    var user : LGUserModel!
}
