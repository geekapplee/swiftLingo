//
//  LGLoginModel.swift
//  lingo
//
//  Created by lingo on 2017/9/29.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation
import HandyJSON
struct LGLoginModel: HandyJSON {
    var account: String = ""
    var password: String = ""
}
