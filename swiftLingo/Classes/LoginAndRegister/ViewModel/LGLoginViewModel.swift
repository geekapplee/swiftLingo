//
//  LGLoginViewModel.swift
//  lingo
//
//  Created by lingo on 2017/9/29.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
class LGLoginViewModel : NSObject,LGRequestable{
    //input
    var urlString: String = "/livefor/user/login"
    var type: MethodType = .get
    var parameters: [String : Any] = [:]
}

extension LGLoginViewModel {
    
}

extension LGLoginViewModel {
    func parseResult(_ result: Any) {
        LGLog(result)
        //guard let resultDict = result as? [String:Any] else {return}
        //if resultDict["code"] as! Int == SUCCESS_CODE{
            kNotiCenter.post(name: NSNotification.Name(rawValue: changeRootToTabbarKey), object: nil)
//        }else{
//
//        }
    }
}
