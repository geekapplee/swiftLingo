//
//  LGLoginView.swift
//  lingo
//
//  Created by lingo on 2017/9/25.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class LGLoginView: UIView {
    
    fileprivate let loginTextFieldHight = 55
    
    //账号
    var accountTF : LGLoginTextField = {
        let accountTF = LGLoginTextField.init(placeholder: "请输入账号", fontSize: 22)
        accountTF.clearButtonMode = .whileEditing
        return accountTF
    }()
    //密码
    lazy var passwordTF : LGLoginTextField = {
        let passwordTF = LGLoginTextField.init(placeholder: "请输入密码", fontSize: 22)
        passwordTF.clearButtonMode = .whileEditing
        passwordTF.isSecureTextEntry = true
        return passwordTF
    }()
    //登录按钮
    var loginBtn : UIButton = UIButton.init(type: .custom)
    //注册按钮
    var registerBtn : UIButton = UIButton.init(type: .custom)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LGLoginView {
    func setupUI() {
        //账号
        addSubview(accountTF)
        accountTF.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(loginTextFieldHight)
        }
        //分割线
        let accountSep = UIView.init()
        addSubview(accountSep)
        accountSep.backgroundColor = UIColor.white
        accountSep.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(accountTF.snp.bottom)
            make.height.equalTo(1)
        }
        //密码
        addSubview(passwordTF)
        passwordTF.snp.makeConstraints { (make) in
            make.top.equalTo(accountSep.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(loginTextFieldHight)
        }
        //分割线
        let pwdSep = UIView.init()
        addSubview(pwdSep)
        pwdSep.backgroundColor = UIColor.white
        pwdSep.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(passwordTF.snp.bottom)
            make.height.equalTo(1)
        }
        //登录按钮
        addSubview(loginBtn)
        loginBtn.backgroundColor = UIColor.blue
        loginBtn.setTitle("登录", for: .normal)
        loginBtn.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(pwdSep.snp.bottom).offset(35)
            make.height.equalTo(40)
        }
        loginBtn.layer.cornerRadius = 4
        loginBtn.layer.masksToBounds = true
        loginBtn.alpha = 0.7
        //注册按钮
        addSubview(registerBtn)
        //registerBtn.backgroundColor = UIColor.red
        registerBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        registerBtn.setTitle("注册", for: .normal)
        registerBtn.setTitleColor(UIColor.blue, for: .normal)
        registerBtn.snp.makeConstraints { (make) in
            make.top.equalTo(loginBtn.snp.bottom).offset(15)
            make.height.equalTo(20)
            make.centerX.equalTo(loginBtn)
        }
    }
}


