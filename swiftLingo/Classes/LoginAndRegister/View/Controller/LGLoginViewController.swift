//
//  LGLoginViewController.swift
//  lingo
//
//  Created by lingo on 2017/9/22.
//  Copyright © 2017年 livefor. All rights reserved.
//  登录控制器

import UIKit
import AVFoundation
import MediaPlayer
import SnapKit
import Alamofire
class LGLoginViewController: ViewController {
    //本地Url
    fileprivate lazy var playURL = { () -> URL? in
       // let playURL = Bundle.main.url(forResource: "moments", withExtension: "mp4")
        let playURL = Bundle.main.url(forResource: "moments", withExtension: "mp4")
        return playURL
    }()
    //播放器
    let player : MPMoviePlayerController = MPMoviePlayerController()

    let loginView : LGLoginView = LGLoginView()
    
    var loginViewModel: LGLoginViewModel = LGLoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //绑定viewModel
//        loginView.accountTF.rx.text.orEmpty.bind(to: loginViewModel.account).disposed(by: bag)
//        loginView.passwordTF.rx.text.orEmpty.bind(to: loginViewModel.password).disposed(by: bag)
        
        //播放器
        setupPlayer()
        //界面
        setupLoginUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //析构
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.MPMoviePlayerLoadStateDidChange, object: nil)
    }
}

// MARK: - 播放器相关代码
extension LGLoginViewController {
    ///设置播放器
    func setupPlayer(){
        player.contentURL = self.playURL
        view.addSubview(player.view)
        player.shouldAutoplay = true
        player.controlStyle = .none
        player.repeatMode = .one
        player.view.frame = view.bounds
        player.view.alpha = 0
        UIView.animate(withDuration: 2) {
            self.player.view.alpha = 1
            self.player.prepareToPlay()
        }
        //监听播放完成
        NotificationCenter.default.addObserver(self, selector:#selector(replay) , name: NSNotification.Name.MPMoviePlayerLoadStateDidChange , object: nil)
    }
    /// 重播
    @objc func replay() {
        let playbackState = self.player.playbackState
        if playbackState == .stopped || playbackState == .paused {
            self.player .play()
        }
    }
}

// MARK: - 设置登录相关
extension LGLoginViewController {
    func setupLoginUI() {
        view.addSubview(loginView)
       // loginView.backgroundColor = randomColor()
        loginView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(235)
            make.top.equalToSuperview().offset(175)
        }
        loginView.loginBtn.addTarget(self, action: #selector(loginBtnClick), for: .touchUpInside)
        loginView.registerBtn.addTarget(self, action: #selector(registerBtnClick), for: .touchUpInside)
    }
    
    
    
    @objc func loginBtnClick(){
        let accont = loginView.accountTF.text
        let password = loginView.passwordTF.text
        
        loginViewModel.parameters = [
            "account":accont ?? 0,
            "password":password ?? 0
        ]
        //发送登录请求
        loginViewModel.requestData {
            
        }
    }
    

    @objc func registerBtnClick() {
        navigationController?.pushViewController(LGRegisterPhoneViewController(), animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}



