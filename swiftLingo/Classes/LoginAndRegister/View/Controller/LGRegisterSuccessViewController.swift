//
//  LGRegisterSuccessViewController.swift
//  swiftLingo
//
//  Created by lingo on 2017/10/8.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGRegisterSuccessViewController: ViewController {
    @IBOutlet weak var loginBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        loginBtn.rx.tap.subscribe(onNext:{[weak self] in
            self?.navigationController?.popToRootViewController(animated: true)
        }).addDisposableTo(bag)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
