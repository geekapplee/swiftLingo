//
//  LGRegisterPhoneViewController.swift
//  swiftLingo
//
//  Created by lingo on 2017/10/8.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit


class LGRegisterPhoneViewController: ViewController {

    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var nextStepBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        phoneTF.setUnderline(color: UIColor.red)
        nextStepBtn.rx.tap.subscribe(onNext: { [weak self] in
            let nicknameVC = LGRegisterNicknameViewController()
            self?.navigationController?.pushViewController(nicknameVC, animated: true)
        }).addDisposableTo(bag)
    }
}
