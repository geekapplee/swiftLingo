//
//  LGRegisterNicknameViewController.swift
//  swiftLingo
//
//  Created by lingo on 2017/10/8.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LGRegisterNicknameViewController: ViewController {
    
    @IBOutlet weak var verCodeTF: UITextField!
    @IBOutlet weak var nicknameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var nextstepBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nextstepBtn.rx.tap.subscribe(onNext:{ [weak self] in
            print("点击了下一步")
            let registerSuccessVC = LGRegisterSuccessViewController()
            self?.navigationController?.pushViewController(registerSuccessVC, animated: true)
            }).addDisposableTo(bag)
    }
}
