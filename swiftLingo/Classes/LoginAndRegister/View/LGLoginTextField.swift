//
//  LGLoginTextField.swift
//  lingo
//
//  Created by lingo on 2017/9/25.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGLoginTextField: UITextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LGLoginTextField {
    convenience init(placeholder: String, fontSize: CGFloat) {
        self.init(frame:.zero)
        self.placeholder = placeholder
        self.font = UIFont.boldSystemFont(ofSize: fontSize)
        self.setValue(placeholderColor, forKeyPath: "_placeholderLabel.textColor")
        self.backgroundColor = UIColor.clear
        self.textColor = UIColor.white
        self.rightViewMode = .whileEditing
    }
}
 
