//
//  LGProfileCell.swift
//  swiftLingo
//
//  Created by lingo on 2017/12/8.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGProfileCell: UITableViewCell,LGReusable {
    private let margin = 8
    
    lazy var rightMoreView = { () -> UIImageView in
        let rightMoreView = UIImageView.init()
        rightMoreView.setIcon(icon: .iconfont(.rightMore), textColor: UIColor.red, size: CGSize(width:20 , height: 20))
        return rightMoreView
    }()
    
    private lazy var iconImgView = { () -> UIImageView in
        let iconImgView = UIImageView.init()
        iconImgView.backgroundColor = randomColor()
        iconImgView.layer.cornerRadius = 4
        iconImgView.layer.masksToBounds = true
        return iconImgView
    }()
    private lazy var userLabel = { () -> UILabel in
        let userLabel = UILabel.init()
        userLabel.text = randomText(5)
        return userLabel
    }()
    private lazy var descLabel = { () -> UILabel in
        let descLabel = UILabel.init()
        descLabel.text = randomText(10)
        return descLabel
    }()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //subview
        contentView.addSubview(rightMoreView)
        contentView.addSubview(iconImgView)
        contentView.addSubview(userLabel)
        contentView.addSubview(descLabel)
        //layout
        rightMoreView.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.right.equalTo(-margin)
            make.centerY.equalToSuperview()
        }
        iconImgView.snp.makeConstraints { (make) in
            make.top.equalTo(margin)
            make.bottom.equalTo(-margin)
            make.left.equalTo(margin)
            make.width.equalTo(iconImgView.snp.height)
        }
        userLabel.snp.makeConstraints { (make) in
            make.left.equalTo(iconImgView.snp.right).offset(margin)
            make.bottom.equalTo(iconImgView.snp.centerY)
        }
        descLabel.snp.makeConstraints { (make) in
            make.top.equalTo(iconImgView.snp.centerY)
            make.left.equalTo(userLabel)
        }
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
