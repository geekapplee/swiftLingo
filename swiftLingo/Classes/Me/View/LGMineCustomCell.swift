//
//  LGMineCustomCell.swift
//  swiftLingo
//
//  Created by lingo on 2017/12/11.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGMineCustomCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        configUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LGMineCustomCell {
    func configUI() -> Void {
        
    }
}
