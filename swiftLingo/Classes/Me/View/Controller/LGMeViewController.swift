//
//  LGMeViewController.swift
//  lingo
//
//  Created by lingo on 2017/9/26.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
class LGMeViewController: UIViewController {

    fileprivate lazy var mainTableView = { () -> UITableView in
        let mainTableView = UITableView.init(frame: .zero, style: .plain)
        mainTableView.backgroundColor = bgColor
        mainTableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: kTabBarH, right: 0)
        mainTableView.dataSource = self
        mainTableView.delegate = self
        return mainTableView
    }()
    
    fileprivate lazy var mineVM = LGMeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "我的"
        view.addSubview(self.mainTableView)
        mainTableView.frame = view.bounds
        self.mainTableView.registerReusable(cellType: LGProfileCell.self)
    }
}
extension LGMeViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mineVM.listArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionArray = self.mineVM.listArray[section]
        return sectionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if 0==indexPath.section {
//            let cell = tableView.dequeueReusableCell(withCellType: LGProfileCell.self, indexPath: indexPath)
//            return cell
//        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellId")
        if nil == cell {
            cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "cellId")
        }
        
        let sectionArray = self.mineVM.listArray[indexPath.section]
        let cellDict: [String:Any] = sectionArray[indexPath.row]
       // let icon: FontType = (cellDict["icon"] as? FontType)!
        let cellName = cellDict["title"]
        cell?.accessoryType = .disclosureIndicator
        cell?.imageView?.setIcon(icon:.iconfont(.dongtai), textColor: UIColor.blue, backgroundColor: UIColor.lightGray,size: CGSize(width: 30, height: 30))
        cell?.textLabel?.text = cellName as? String
        
        //cell?.backgroundColor = randomColor()
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView.init()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
}

extension LGMeViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if 0 == indexPath.section {
            return 88
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 22
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}

