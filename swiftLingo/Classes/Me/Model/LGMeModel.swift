//
//  LGMeModel.swift
//  swiftLingo
//
//  Created by lingo on 2017/12/8.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation
enum MeCellType {
    case MeCellTypeProfile
    case MeCellTypeSwitch
    case MeCellTypeMore
}

struct LGMeModel {
    var cellType: MeCellType = .MeCellTypeMore // 默认为更多
    var cellIcon: String = ""
    var cellName: String = ""
    var cellValue: String = ""
    var navSel: String = ""
}
