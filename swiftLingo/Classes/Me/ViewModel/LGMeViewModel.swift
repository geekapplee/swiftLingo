//
//  LGMeViewModel.swift
//  swiftLingo
//
//  Created by lingo on 2017/12/8.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGMeViewModel: NSObject {
    
    //数据组装
    lazy var listArray:Array = {
        return [
                //第一组
                    [
                        ["icon":"","title":"李林果","selector":""],
//                        ["icon":"iconName","title":"titleName","selector":""],
//                        ["icon":"iconName","title":"titleName","selector":""]
                    ],
                //第二组
                    [
                        ["icon":"","title":"收藏","selector":""],
                        ["icon":"","title":"修改密码","selector":""],
                        ["icon":"","title":"一键叫妈","selector":""]
                    ],
                //第三组
                    [
                        ["icon":"","title":"联系我们","selector":""],
                        ["icon":"","title":"清除缓存","selector":""],
                        ["icon":"","title":"退出登录","selector":""]
                    ]
            ]
    }()
}

extension LGMeViewModel {
    
}
