//
//  LGHomeItemModel.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/10.
//  Copyright © 2017年 livefor. All rights reserved.
//

import Foundation
struct LGHomeItemModel {
    var itemId : Int
    var itemImg : String
    var itemTitle : String
    var itemBadge : Int
    var itemDetail : String
}
