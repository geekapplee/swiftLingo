//
//  LGHomeViewController.swift
//  lingo
//
//  Created by lingo on 2017/9/26.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGHomeViewController: ViewController {
    
    let cellMargin: CGFloat = 5.0
    var isGrid: Bool = true
    
    fileprivate lazy var homeCollectionView = { () -> UICollectionView in 
        let layout = UICollectionViewFlowLayout.init()
        let homeCollectionView = UICollectionView.init(frame:.zero, collectionViewLayout: layout)
        homeCollectionView.showsVerticalScrollIndicator = false
        homeCollectionView.showsHorizontalScrollIndicator = false
        homeCollectionView.backgroundColor = .clear
        homeCollectionView.dataSource = self
        homeCollectionView.delegate = self
        return homeCollectionView
    }()
    
    fileprivate lazy var homeVM = LGHomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeVM.fomatData()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "翻转", target: self, action: #selector(gridBtnClick(sender:)))
        
        setupHomeCollection()
    }

    func setupHomeCollection(){
        view.addSubview(self.homeCollectionView)
        self.homeCollectionView.registerReusable(cellType: LGHomeItemCell.self)
        homeCollectionView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalTo(self.view)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension LGHomeViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(cellMargin, cellMargin, cellMargin, cellMargin)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemH = (kScreenW-cellMargin*4)/3 //isGrid ? (kScreenW-cellMargin*4)/3 : (kScreenW - cellMargin*2)
        let itemW = isGrid ? (kScreenW-cellMargin*4)/3 : (kScreenW - cellMargin*2)
        return CGSize(width: itemW, height: itemH)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }
}

extension LGHomeViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let testVC = LGTestViewController()
        self.navigationController?.pushViewController(testVC, animated: true)
    }
}

extension LGHomeViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeVM.listArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withCellType: LGHomeItemCell.self, indexPath: indexPath)
        cell.isGrid = isGrid
        cell.backgroundColor = UIColor.lightText
        let model = homeVM.listArray[indexPath.row]
        cell.itemLabel.text = model.itemTitle
        cell.itemImg.image = UIImage.init(named: model.itemImg);
        cell.detailLabel.text = model.itemDetail
        return cell
    }
}

extension LGHomeViewController {
    @objc func gridBtnClick(sender: UIButton){
        isGrid = !isGrid
        homeCollectionView.reloadData()
    }
}



