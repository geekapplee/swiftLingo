//
//  LGHomeItemCell.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/10.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGHomeItemCell: UICollectionViewCell,LGReusable {
    fileprivate let labelH: CGFloat = 30
    lazy var itemLabel = { () -> UILabel in
        let itemLabel = UILabel.init()
       // itemLabel.backgroundColor = UIColor.init(white: 0, alpha: 0.1)
        itemLabel.textAlignment = .center
        itemLabel.font = UIFont.systemFont(ofSize: 15)
        return itemLabel
    }()
    
    lazy var itemImg = { () -> UIImageView in
        let itemImg = UIImageView.init()
        return itemImg
    }()
    
    lazy var detailLabel = { () -> UILabel in
        let detailLabel = UILabel.init()
        detailLabel.font = UIFont.systemFont(ofSize: 20)
        detailLabel.numberOfLines = 0
        return detailLabel
    }()
    
    var isGrid:Bool?{
        didSet{
            self.detailLabel.isHidden = isGrid!;
        }
    }
    
    var itemModel:LGHomeItemModel!{
        didSet{
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(itemImg)
        addSubview(itemLabel)
        addSubview(detailLabel)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        itemImg.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.bottom.equalTo(-30)
            make.left.equalTo(10)
            make.width.equalTo(itemImg.snp.height)
        }
        itemLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(itemImg)
            make.bottom.equalToSuperview()
            make.height.equalTo(30)
        }
        detailLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalTo(itemImg.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
            make.bottom.lessThanOrEqualToSuperview().offset(-10)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
