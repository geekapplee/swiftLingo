//
//  LGDrawingBoardViewController.swift
//  swiftLingo
//
//  Created by lingo on 2017/12/5.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGDrawingBoardViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = bgColor

        let rect = CGRect.init(x: 10, y: 10, width: view.frame.size.width - 20, height: view.frame.size.height - 20)
        let drawingBoard = LGDrawingBoardView.init(frame: rect)
        drawingBoard.backgroundColor = UIColor.lightGray
        view.addSubview(drawingBoard)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
