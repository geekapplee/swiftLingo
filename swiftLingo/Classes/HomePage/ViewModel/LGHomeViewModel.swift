//
//  LGHomeViewModel.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/10.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit

class LGHomeViewModel: NSObject {
//构建数据
    lazy var listArray = [LGHomeItemModel]()
    func fomatData() -> Void {
        let item0 = LGHomeItemModel(itemId: 0, itemImg: "myMa", itemTitle: "科学备孕", itemBadge: 0, itemDetail:"备孕是指孕妇或其家人对优孕的前提准备，孕前调理分为“身”、“心”两方面。中医强调五脏六腑要协调、阴阳平衡 ，不可过与不及，气血须充足，如准妈妈要多吃叶酸，准爸爸要多吃富含锌的食物等。备孕是优孕的关键，却往往最容易被忽略")
        let item1 = LGHomeItemModel(itemId: 1, itemImg: "dietSafe", itemTitle: "饮食安全", itemBadge: 0, itemDetail:"指食品无毒、无害，符合应当有的营养要求，对人体健康不造成任何急性、亚急性或者慢性危害。根据倍诺食品安全定义，食品安全是“食物中有毒、有害物质对人体健康影响的公共卫生问题”。")
        let item2 = LGHomeItemModel(itemId: 2, itemImg: "singsong", itemTitle: "诗词歌赋", itemBadge: 0, itemDetail:"所谓诗词歌赋，是人们对我国传统文学的概称；虽然如此，这一称谓几乎可说是业已概括了中国传统文化的精髓和文化尤其是传统文学的大成。")
        let item3 = LGHomeItemModel(itemId: 3, itemImg: "aoshu", itemTitle: "奥数竞赛", itemBadge: 0, itemDetail:"孩子在上小学后就要抓数学的学习，一刻也不能放松，尤其是到了小学三、四年级时，除了学好课内的学习内容外，细心的家长一定要个给孩子报个奥数培训班")
        let item4 = LGHomeItemModel(itemId: 4, itemImg: "jiaohu", itemTitle: "日常教辅", itemBadge: 0, itemDetail:"这是详情")
        let item5 = LGHomeItemModel(itemId: 5, itemImg: "dawingPad", itemTitle: "妈宝画板", itemBadge: 0, itemDetail:randomText())
        let item6 = LGHomeItemModel(itemId: 6, itemImg: "gosafe", itemTitle: "出行安全", itemBadge: 0, itemDetail:randomText())
        
        listArray.append(item0)
        listArray.append(item1)
        listArray.append(item2)
        listArray.append(item3)
        listArray.append(item4)
        listArray.append(item5)
        listArray.append(item6)
    }
}
