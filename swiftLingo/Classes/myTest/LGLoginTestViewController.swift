//
//  LGLoginTestViewController.swift
//  lingo
//
//  Created by lingo on 2017/9/22.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LGLoginTestViewController: UIViewController {

    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var usernameNote: UILabel!
    
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var passwordNote: UILabel!
    
    @IBOutlet weak var loginBtn: UIButton!

    var bag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let usernameValid = usernameTF.rx.text.orEmpty.map({$0.characters.count>=6}).shareReplay(1)
        
        let passwordValid = passwordTF.rx.text.orEmpty.map({$0.characters.count>=6}).shareReplay(1)
        
        let comValid = Observable.combineLatest(usernameValid, passwordValid) { (usernameV, passwordV) -> Bool in
            return usernameV && passwordV
            }.shareReplay(1)
        
        usernameValid.bind(to: usernameNote.rx.isHidden).disposed(by: bag)
        
        passwordValid.bind(to: passwordNote.rx.isHidden).disposed(by: bag)
        
        comValid.bind(to: loginBtn.rx.isEnabled).disposed(by: bag)
        loginBtn.rx.tap.subscribe(onNext: {[weak self] in
            print("aaaa")
        }).disposed(by:bag)
        
        let yourView = UIView.init()
        view.addSubview(yourView)
        yourView.frame = CGRect(x: 100, y: loginBtn.frame.maxY + 10, width: 100, height: 44)
        yourView.backgroundColor = UIColor.init(hexString: "ff0000")
//       yourView.backgroundColor = UIColor.init(hexString: "#f00")
//        yourView.backgroundColor = UIColor.init(hexNumber: 0xff0000)
        
      //  yourView.backgroundColor = UIColor.init(hexNumber: 0xff0000)
    }
}
