//
//  LGTestViewModel.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/24.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import Qiniu
class LGTestViewModel: NSObject,LGRequestable{
    var urlString: String = "/livefor/qiniu"
    
    var type: MethodType = .get
    
    var parameters: [String : Any] = [:]
    
    var imgData:Data?
    
    func parseResult(_ result: Any) {
        var result = result as! [String:String]
        let token = result["token"]
    
        let uploadManager = QNUploadManager()
        
        uploadManager?.put(imgData, key: nil, token: token, complete: { (info, key, resp) in
            LGLog(info)
            LGLog(key)
            LGLog(resp)
        }, option: nil)
    }
}
