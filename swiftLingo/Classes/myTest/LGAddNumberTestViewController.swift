//
//  LGAddNumberTestViewController.swift
//  lingo
//
//  Created by lingo on 2017/9/22.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LGAddNumberTestViewController: UIViewController {
    @IBOutlet weak var numberText1: UITextField!
    @IBOutlet weak var numberText2: UITextField!
    @IBOutlet weak var textLabel: UILabel!

    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Observable.combineLatest(numberText1.rx.text.orEmpty, numberText2.rx.text.orEmpty) { (text1, text2) -> Int in
            return (Int(text1) ?? 0) + (Int(text2) ?? 0)
            }.map{"\($0)"}
            .bind(to: textLabel.rx.text)
            .disposed(by: disposeBag);
        }
}
