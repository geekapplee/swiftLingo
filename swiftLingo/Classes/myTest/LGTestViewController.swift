//
//  LGTestViewController.swift
//  swiftLingo
//
//  Created by lingo on 2017/11/21.
//  Copyright © 2017年 livefor. All rights reserved.
//

import UIKit
import Gallery
import Lightbox

class LGTestViewController: UIViewController,LGSharable {
    fileprivate lazy var gallery = { () -> GalleryController in
        Gallery.Config.VideoEditor.savesEditedVideoToLibrary = true
        let gallery = GalleryController()
        gallery.delegate = self
        return gallery
    }()
    
    fileprivate lazy var galleryTestBtn = { () -> UIButton in
        let btn = UIButton.init(type: .custom)
        btn.backgroundColor = UIColor.red
        btn.frame = CGRect(x: 10, y: 100, width: self.view.lg_width - 20, height: 50)
        btn.setTitle("测试gallery", for: .normal)
        btn.addTarget(self, action:#selector(btnClick), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var shareBtn = { () -> UIButton in
        let btn = UIButton.init(type: .custom)
        btn.backgroundColor = UIColor.red
        btn.frame = CGRect(x: 10, y: 160, width: self.view.lg_width - 20, height: 50)
        btn.setTitle("测试wxShare", for: .normal)
        btn.addTarget(self, action:#selector(shareBtnClick), for: .touchUpInside)
        return btn
    }()

    
    fileprivate lazy var imgView = { () -> UIImageView in
        let imgView = UIImageView.init(frame: CGRect.init(x: 10, y: 170, width: view.lg_width - 20, height: view.lg_width - 20))
        imgView.backgroundColor = UIColor.lightGray
        return imgView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = bgColor
        
//        view.addSubview(galleryTestBtn)
//        view.addSubview(shareBtn)
//        
        //view.addSubview(imgView)
    }
    
    @objc func btnClick() -> Void {
        present(self.gallery, animated: true) {}
    }
    
    @objc func shareBtnClick(){

        shareImg(image: UIImage.init(named: "singsong")!, inScene:WXSceneTimeline)
        
    }
}
//分享
extension LGTestViewController{
    
//    @discardableResult
//    //文字
//    func shareText(text:String, inScene:WXScene) -> Bool{
//        let req = SendMessageToWXReq()
//        req.text = text
//        req.bText = true
//        req.scene = Int32(inScene.rawValue)
//        return WXApi.send(req)
//    }
    @discardableResult
    func shareImg(image:UIImage, inScene:WXScene) -> Bool {
        let imgObj = WXImageObject()
        imgObj.imageData = UIImagePNGRepresentation(image)
        
        let msg = WXMediaMessage()
        msg.title = "我是title"
        msg.description = "我是description"
        msg.mediaObject = imgObj
        msg.mediaTagName = "tagNameHH"
        
        UIGraphicsBeginImageContext(CGSize(width: 100, height: 100))
        image.draw(in: CGRect(x: 0, y: 0, width: 100, height: 100))
        let thumbImage=UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        msg.thumbData = UIImagePNGRepresentation(thumbImage!)
        
        let req = SendMessageToWXReq()
        req.message = msg
        req.text = "reqText"
        req.bText = false
        req.scene = Int32(inScene.rawValue)
        return WXApi.send(req)
    }
    
}

extension LGTestViewController: GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
       let img = images.first
        let uiImg = img?.uiImage(ofSize: UIScreen.main.bounds.size)
        imgView.image = uiImg
//        LGLog("1")
        let testVM = LGTestViewModel.init()
        testVM.imgData = UIImageJPEGRepresentation(uiImg!, 0.1)
        testVM.requestData {
        
        }
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
        LGLog("2")
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        LightboxConfig.DeleteButton.enabled = true
     let lightboxImages = images.flatMap { $0.uiImage(ofSize: UIScreen.main.bounds.size) }.map({ LightboxImage(image: $0) })
         guard lightboxImages.count == images.count else {
         return
         }
         
         let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
         lightbox.dismissalDelegate = self
         
         controller.present(lightbox, animated: true, completion: nil)
        
        LGLog("3")
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        LGLog("4")
    }
}
extension LGTestViewController: LightboxControllerDismissalDelegate {
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
}

